
Ubuntu Workstation Setup

Install Ubuntu 12.04 32 bit from http://www.ubuntu.org




Note: Package list determined through experimentation as I attempted to resolve all the dependencies required in the JOGL tutorial.  I am attempting to use the NEWT example.

sudo apt-get install default-jdk
sudo apt-get install libjogl2-java libjogl2-toolkits libjava3d-java
sudo apt-get install ant

Base reference

https://sites.google.com/site/justinscsstuff/jogl-tutorial-1

ANT Documentation
http://ant.apache.org/manual/using.html
