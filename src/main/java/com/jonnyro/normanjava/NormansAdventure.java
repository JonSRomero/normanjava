package com.jonnyro.normanjava;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import javax.swing.*;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLException;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureCoords;
import com.jogamp.opengl.util.texture.TextureIO;
import static javax.media.opengl.GL.*;  // GL constants
import static javax.media.opengl.GL2.*; // GL2 constants



public class Sprite {
    private string texture_name;
    private Texture
}

@SuppressWarnings("serial")
public class NormansAdventure extends GLCanvas implements GLEventListener {
    private static String TITLE = "NeHe Lesson #6: Texture";
    private static final int CANVAS_WIDTH = 320; // width of the drawable
    private static final int CANVAS_HEIGHT = 240; // height of the drawable
    private static final int FPS = 60; // animator's target fps
    
    /** The entry main() method to setup the top-level container and animator */
    
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // Run the GUI codes in the event-dispatching thread for thread safety
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                //Create the OpenGL rendering canvas
                GLCanvas canvas = new NormansAdventure();
                canvas.setPreferredSize( new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
                
                // Create an animator that drives canvas' display() at the specified FPS
                final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);
                
                //Create the top-level container
                final JFrame frame = new JFrame(); // Swing's JFrame or AWT's Frame
                frame.getContentPane().add(canvas);
                frame.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        // Use a dedicated thread to run the stop() to ensure the animator
                        // thread stops before program exists.
                        new Thread() {
                            @Override
                            public void run() {
                                if (animator.isStarted()) animator.stop();
                                System.exit(0);
                            }
                        }.start();
                    }
                });
                frame.setTitle(TITLE);
                frame.pack();
                frame.setVisible(true);
                animator.start(); // Start the animation loop
            }
        });
    }

    // Setup OpenGL Graphics Renderer
    private GLU glu; // for the GL Utility
    
    //Rotational angle about the x, y, and z axes in degrees
    private static float angleX = 0.0f;
    private static float angleY = 0.0f;
    private static float angleZ = 0.0f;
    
    //Rotation speed about x, y, z axes in degrees per refresh
    private static float rotateSpeedX = 0.3f;
    private static float rotateSpeedY = 0.2f;
    private static float rotateSpeedZ = 0.4f;
    
    //Texture
    private Texture texture;
    private String textureFileName = "texture_sheet.png";
    private String textureFileType = ".png";
    
    // Texture image flips vertically. Shall use TextureCoords class to 
    // retrieve the top, bottom, left, and right coordinates.
    
    private float textureTop, textureBottom, textureLeft, textureRight;
    
    /** Constructor to set up the GUI for this component */
    
    public NormansAdventure() {
        this.addGLEventListener(this);
    }
    
    // ------ Implement methods declared in GLEventListerner ------
    
    /**
     * Called back immediately after the OpenGL context is initialized.  Can be used
     * to perform one-time initialization. Run only once.
     * 
     */
    @Override
    public void init(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        
        glu = new GLU();
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glClearDepth(1.0f);
        gl.glEnable(GL_DEPTH_TEST); // Enables depth testing
        gl.glDepthFunc(GL_LEQUAL); // the type of depth test to do
        gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // best perspective coorection
        
        gl.glShadeModel(GL_SMOOTH); // blends colors nicely, and smooths out lighting
        
        //Load Texture from image
        try {
            // Create an OpenGL texture object from (URL, mimpmap, file suffix)
            // Use URL so that can read from JAR and disk file.
            texture = TextureIO.newTexture(
                    getClass().getClassLoader().getResource(textureFileName), // relative to project root
                    false, textureFileType);
            
            // Use linear filter fo texture if image is larger than the original texture
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            
            // Texture image flips vertically. Shall use TextureCoords class to retrieve
            // the top, bottom, left and right coordinates, instead of using 0.0f and 1.0f.
            TextureCoords textureCoords = texture.getImageTexCoords();
            textureTop = textureCoords.top();
            textureBottom = textureCoords.bottom();
            textureLeft = textureCoords.left();
            textureRight = textureCoords.right();
            
        } catch (GLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
       
        GL2 gl = drawable.getGL().getGL2(); // Get the OpenGL Graphics context
        
        if (height == 0) height = 1; // prevent divide by zero
        float aspect = (float)width / height;

        // Set the view port (display area) to cover the entire window
        gl.glViewport(0, 0, width, height);

        // Setup perspective projection, with aspect ratio matches viewport
        gl.glMatrixMode(GL_PROJECTION); // Choose projection matrix
        gl.glLoadIdentity();            // reset projection matrix
        glu.gluPerspective(45.0, aspect, 0.1, 100.0); // fovy, aspect, zNear, zFar

        // Enable the model-view transform
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity();            // reset
     
    }

    /**
      * Called back by the animator to perform rendering
    */

    @Override
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2(); // get the OpenGL 2 graphics context
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear color and depth buffers
        gl.glLoadIdentity(); // reset the model-view matrix

        // ------ Bind texture ------
        texture.enable(gl);
        texture.bind(gl);

        // ----- Your OpenGL rendering code here ( Render a white triangle for testing ) -----
        gl.glTranslatef(0.0f, 0.0f, -6.0f); // translate into the screen
        gl.glBegin(GL_QUADS); // draw using quads 
            gl.glTexCoord2f(textureLeft, textureBottom);
            gl.glVertex3f(0.0f, 0.0f, 0.0f);

            gl.glTexCoord2f(textureRight, textureTop);
            gl.glVertex3f(1.0f, 0.0f, 0.0f);

            gl.glTexCoord2f(textureRight, textureTop);
            gl.glVertex3f(1.0f, 1.0f, 0.0f);

            gl.glTexCoord2f(textureLeft, textureTop);
            gl.glVertex3f(0.0f, 1.0f, 0.0f);
        gl.glEnd();
    }

    /**
     *  Called back before the OpenGL context is destroyed. Release resource such as buffers
     */    
    @Override
    public void dispose(GLAutoDrawable drawable) { }

}
